<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
</head>
<body>
    <div id="content">

    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            loadData();
        });

        function loadData() {
            $.get('data.php', function(data){
                $('#content').html(data);
            })
        }
    </script>
</body>
</html>
<!-- https://www.youtube.com/watch?v=0hyn6iQBLJU -->